package com.apirest.apirest.controllers;

import com.apirest.apirest.models.UsuarioModel;
import com.apirest.apirest.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;


    @GetMapping()
    public ArrayList<UsuarioModel> obtenerUsuarios()
    {
        return usuarioService.obtenerUsuarios();
    }

    @PostMapping()
    public String guardarUsuario(@RequestBody UsuarioModel usuarioModel)
    {
        return usuarioService.guardarUsuario(usuarioModel);
    }

    @PutMapping()
    public  UsuarioModel updateUsuario(@RequestBody UsuarioModel usuarioModel)
    {
        return usuarioService.updateUsuario(usuarioModel);
    }

    @GetMapping(path = "/{id}")
    public Optional<UsuarioModel> obtenerUsuarioPorId(@PathVariable("id") Integer id)
    {
        return usuarioService.obtenerById(id);
    }

    @DeleteMapping(path = "/{id}")
    public String eliminarPorId(@PathVariable("id") Integer id)
    {
        boolean ok = usuarioService.eliminarUsuario(id);
        if(ok){
            return "Se eliminó el usuario con id " + id;
        }else{
            return "No pudo eliminar el usuario con id "+ id;
        }
    }


}
