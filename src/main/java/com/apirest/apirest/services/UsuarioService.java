package com.apirest.apirest.services;

import com.apirest.apirest.models.UsuarioModel;
import com.apirest.apirest.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    UsuarioRepository usuarioRepository;

    //obtener todos los usuarios
    public ArrayList<UsuarioModel> obtenerUsuarios()
    {
        return (ArrayList<UsuarioModel>) usuarioRepository.findAll();
    }

    //guardar un usuario
    public String guardarUsuario(UsuarioModel usuarioModel)
    {
        try {
            UsuarioModel usuariop = new UsuarioModel();
            usuariop.setNombres(usuarioModel.getNombres());
            usuariop.setApellidos(usuarioModel.getApellidos());
            usuariop.setCelular(usuarioModel.getCelular());
            usuariop.setPassword(encoder.encode(usuarioModel.getPassword()));
            usuarioRepository.save(usuariop);
            return "Usuario agregado correctamente";
        } catch (Exception error)
        {
            return "Error!! verifique datos enviados";
        }

    }

    //update usuario
    public UsuarioModel updateUsuario(UsuarioModel usuarioModel)
    {
        usuarioModel.setPassword(encoder.encode(usuarioModel.getPassword()));
        return usuarioRepository.save(usuarioModel);
    }

    //obtener un usuario por id
    public Optional<UsuarioModel> obtenerById(Integer id)
    {

        return usuarioRepository.findById(id);
    }


    //eliminar un usuario
    public boolean eliminarUsuario(Integer id)
    {
        try {
            usuarioRepository.deleteById(id);
            return true;
        } catch (Exception err){
            return false;
        }
    }


}
